# Atlassian Maven Enforcer Rules

These are a collection of enforcer rules written by [Atlassian](http://www.atlassian.com/) for the [Maven Enforcer Plugin](http://maven.apache.org/enforcer/maven-enforcer-plugin/) or The Loving Iron Fist of Maven™

## Available Rules
* [File Contains Rule](src/master/docs/FileContainsRule.md)
* [Ban Dependencies By Version](src/master/docs/BanVersionDeps.md)

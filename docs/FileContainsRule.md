# File Contains Enforcer Rule

This is a rule for the [Maven Enforcer Plugin](http://maven.apache.org/enforcer/maven-enforcer-plugin/) or The Loving Iron Fist of Maven™

Runs in maven 2 or 3 under maven-enforcer-plugin 1.3.1 or later.

A file and a plain string or a [Java Regular Expression](http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html#sum) are specified and if the file does not match with the specified string or pattern, the build will fail.

## Parameters

**file** *mandatory* the file to be checked. You can use variables like ${project.basedir} to get the project path

**containsPattern** *optional* a pattern to look for inside the file

**containsString** *optional* a static string to look for inside the file

## Sample Configuration

    <project>
    [...]
        <build>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven.enforcer.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>com.atlassian.maven.enforcer</groupId>
                            <artifactId>maven-enforcer-rules</artifactId>
                            <version>${atlassian.enforcer.rules.version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <id>enforce</id>
                            <configuration>
                                <rules>
                                    <fileContainsRule
                                        implementation="com.atlassian.maven.enforcer.FileContainsRule">
                                        <file>${project.basedir}/src/main/webapp/WEB-INF/classes/log4j.properties</file>
                                        <containsPattern>log4j\.rootLogger\s*=\s*WARN\b</containsPattern>
                                    </fileContainsRule>
                                </rules>
                            </configuration>
                            <goals>
                                <goal>enforce</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    [...]
    </project>

## Sample Output

    [WARNING] Rule 0: com.atlassian.maven.enforcer.FileContainsRule failed with message:
    Pattern FAILPATTERN was not found in file /SuperSlowDisk/src/atlassian/confluence-distribution/common-war/src/main/webapp/WEB-INF/classes/log4j.properties
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD FAILURE
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1.087s
    [INFO] Finished at: Thu May 08 17:36:29 EST 2014
    [INFO] Final Memory: 7M/180M
    [INFO] ------------------------------------------------------------------------
    [ERROR] Failed to execute goal org.apache.maven.plugins:maven-enforcer-plugin:1.3.1:enforce (enforce) on project confluence-common-war-distribution: Some Enforcer rules have failed. Look above for specific messages explaining why the rule failed. -> [Help 1]
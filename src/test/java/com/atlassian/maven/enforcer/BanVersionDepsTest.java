package com.atlassian.maven.enforcer;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.apache.maven.enforcer.rule.api.EnforcerRuleHelper;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BanVersionDepsTest
{
    private BanVersionDeps banVersionDeps;

    private Set<String> reactorGAVs;

    private EnforcerRuleHelper helper;
    private Log log;

    private Set<Artifact> artifacts;
    private Artifact art1;
    private Artifact art2;
    private Artifact art3;

    @Before
    public void before()
    {
        banVersionDeps = new BanVersionDeps();

        reactorGAVs = new HashSet<String>();
        banVersionDeps.reactorGAVs = reactorGAVs;

        helper = mock(EnforcerRuleHelper.class);
        log = mock(Log.class);

        artifacts = new HashSet<Artifact>();

        art1 = mock(Artifact.class);
        when(art1.getGroupId()).thenReturn("grp1");
        when(art1.getArtifactId()).thenReturn("art1");
        when(art1.getType()).thenReturn("typ1");
        when(art1.getVersion()).thenReturn("ver1");
        when(art1.getBaseVersion()).thenReturn("bve1");
        when(art1.getScope()).thenReturn(Artifact.SCOPE_COMPILE);
        artifacts.add(art1);

        art2 = mock(Artifact.class);
        when(art2.getGroupId()).thenReturn("grp2");
        when(art2.getArtifactId()).thenReturn("art2");
        when(art2.getType()).thenReturn("typ2");
        when(art2.getVersion()).thenReturn("ver2");
        when(art2.getBaseVersion()).thenReturn("bve2");
        when(art2.getScope()).thenReturn(Artifact.SCOPE_COMPILE);
        artifacts.add(art2);

        art3 = mock(Artifact.class);
        when(art3.getGroupId()).thenReturn("grp3");
        when(art3.getArtifactId()).thenReturn("art3");
        when(art3.getType()).thenReturn("typ3");
        when(art3.getVersion()).thenReturn("ver3");
        when(art3.getBaseVersion()).thenReturn("bve3");
        when(art2.getScope()).thenReturn(Artifact.SCOPE_COMPILE);
        artifacts.add(art3);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void bannedDependencyVersionRegexpMissing() throws EnforcerRuleException
    {
        expectedException.expect(EnforcerRuleException.class);
        expectedException.expectMessage("missing bannedDependencyVersionRegexp");

        banVersionDeps.execute(helper);
    }

    @Test
    public void bannedDependencyVersionRegexpBad() throws EnforcerRuleException
    {
        expectedException.expect(EnforcerRuleException.class);
        expectedException.expectMessage("invalid bannedDependencyVersionRegexp: '('");

        banVersionDeps.setBannedDependencyVersionRegexp("(");

        banVersionDeps.execute(helper);
    }

    @Test
    public void noFailReactorVersionRegexpBad() throws EnforcerRuleException
    {
        expectedException.expect(EnforcerRuleException.class);
        expectedException.expectMessage("invalid noFailReactorVersionRegexp: '('");

        banVersionDeps.setBannedDependencyVersionRegexp("bleh");
        banVersionDeps.setNoFailReactorVersionRegexp("(");

        banVersionDeps.execute(helper);
    }

    @Test
    public void checkDependenciesBanAll() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = true;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile(".*");

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, containsInAnyOrder(art1, art2, art3));
    }

    @Test
    public void checkDependenciesBanOne() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = true;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile("ver3");

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, contains(art3));
    }

    @Test
    public void checkDependenciesBanNone() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = true;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile("notmatchinganything");

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, hasSize(0));
    }

    @Test
    public void checkDependenciesNoFailBuild() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = false;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile("ver3");

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, hasSize(0));

        verify(log).warn(anyString());
    }

    @Test
    public void checkDependenciesIgnoreTest() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = true;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile(".*");
        banVersionDeps.setIgnoreTest(true);

        when(art1.getScope()).thenReturn(Artifact.SCOPE_TEST);

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, containsInAnyOrder(art2, art3));
    }

    @Test
    public void checkDependenciesIgnoreReactor() throws EnforcerRuleException
    {
        banVersionDeps.failBuild = true;
        banVersionDeps.bannedDependencyVersionPattern = Pattern.compile(".*");
        reactorGAVs.add(art1.getGroupId() + ":" + art1.getArtifactId() + ":" + art1.getVersion());

        when(art1.getScope()).thenReturn(Artifact.SCOPE_TEST);

        Set<Artifact> output = banVersionDeps.checkDependencies(artifacts, log);

        assertThat(output, containsInAnyOrder(art2, art3));
    }

    @Test
    public void shouldFailBuildYes()
    {
        banVersionDeps.setNoFailSnapshots(false);

        MavenProject project = mock(MavenProject.class);
        Artifact artifact = mock(Artifact.class);

        when(project.getArtifact()).thenReturn(artifact);
        when(artifact.isSnapshot()).thenReturn(true);
        when(artifact.getVersion()).thenReturn("blah");

        assertThat(banVersionDeps.shouldFailBuild(project), is(true));
    }

    @Test
    public void shouldFailBuildSnapshotNo()
    {
        banVersionDeps.setNoFailSnapshots(true);

        MavenProject project = mock(MavenProject.class);
        Artifact artifact = mock(Artifact.class);

        when(project.getArtifact()).thenReturn(artifact);
        when(artifact.isSnapshot()).thenReturn(true);
        when(artifact.getVersion()).thenReturn("blah");

        assertThat(banVersionDeps.shouldFailBuild(project), is(false));
    }

    @Test
    public void shouldFailBuildPatternNo()
    {
        banVersionDeps.setNoFailSnapshots(false);
        banVersionDeps.noFailReactorVersionPattern = Pattern.compile("ver");

        MavenProject project = mock(MavenProject.class);
        Artifact artifact = mock(Artifact.class);

        when(project.getArtifact()).thenReturn(artifact);
        when(artifact.isSnapshot()).thenReturn(true);
        when(artifact.getVersion()).thenReturn("ver");

        assertThat(banVersionDeps.shouldFailBuild(project), is(false));
    }

    @Test
    public void includeExcludeNone()
    {
        Set<Artifact> output = banVersionDeps.includeExclude(artifacts);

        assertThat(output, containsInAnyOrder(art1, art2, art3));
    }

    @Test
    public void includeExcludeOnlyIncludes()
    {
        List<String> includes = new ArrayList<String>();
        includes.add("grp1");
        banVersionDeps.setIncludes(includes);

        Set<Artifact> output = banVersionDeps.includeExclude(artifacts);

        assertThat(output, contains(art1));
    }

    @Test
    public void includeExcludeOnlyExcludes()
    {
        List<String> excludes = new ArrayList<String>();
        excludes.add("grp1");
        banVersionDeps.setExcludes(excludes);

        Set<Artifact> output = banVersionDeps.includeExclude(artifacts);

        assertThat(output, containsInAnyOrder(art2, art3));
    }

    @Test
    public void includeExcludeBothIncludesExcludes()
    {
        List<String> excludes = new ArrayList<String>();
        excludes.add("grp1");
        banVersionDeps.setExcludes(excludes);

        List<String> includes = new ArrayList<String>();
        includes.add("grp1");
        includes.add("grp2");
        banVersionDeps.setIncludes(includes);

        Set<Artifact> output = banVersionDeps.includeExclude(artifacts);

        assertThat(output, contains(art2));
    }
}

package com.atlassian.maven.enforcer;

import org.apache.maven.enforcer.rule.api.EnforcerRule;
import org.apache.maven.enforcer.rule.api.EnforcerRuleException;
import org.apache.maven.enforcer.rule.api.EnforcerRuleHelper;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluationException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

@SuppressWarnings("unused")
public class ProjectMustBeSnapshot implements EnforcerRule
{
    String message;

    public void execute(final EnforcerRuleHelper helper) throws EnforcerRuleException
    {
        final MavenProject project = retrieveReactorProject(helper);

        if (!project.getArtifact().isSnapshot())
        {
            throw new EnforcerRuleException("project does not have SNAPSHOT version: " + project.getVersion() + (message != null ? " " + message : ""));
        }
    }

    public String getCacheId()
    {
        return "";
    }

    public boolean isCacheable()
    {
        return false;
    }

    public boolean isResultValid(EnforcerRule arg0)
    {
        return false;
    }

    MavenProject retrieveReactorProject(final EnforcerRuleHelper helper) throws EnforcerRuleException
    {
        try
        {
            return ((MavenProject) helper.evaluate( "${project}" ));
        }
        catch ( ExpressionEvaluationException eee )
        {
            throw new EnforcerRuleException( "Unable to retrieve the MavenProject: ", eee );
        }
    }

    public void setMessage(final String message)
    {
        this.message = message;
    }
}
